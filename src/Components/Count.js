import { Component } from "react";

class Count extends Component{
    constructor(props){
        super(props);

        this.state = {
            count : 1
        }
    }

    addCounter = () =>{
        this.setState({
            count : this.state.count + 1
        })
    }
    
    componentWillMount() {
        console.log("Component Will Mout ");
    }

    componentDidMount(){
        console.log("Component Did Mount ");
        setInterval(() => this.addCounter(),1000); //Truyền vào callback function
    }

    shouldComponentUpdate(){
        console.log('Should Component update');
        return true;
    }

    componentWillUpdate(){
        console.log('Component Will update');
    }

    componentDidUpdate(){
        console.log("Component Did Update");

        if(this.state.count === 10){ //Khi count === 10 xóa component đi 
            this.props.display();
        }
    }

    componentWillUnmount(){
        console.log("Component will unmount");
    }

    render(){
        console.log('Render : count' , this.state.count);

        return(
            <p>Count : {this.state.count}</p>
        )
    }

}

export default Count;